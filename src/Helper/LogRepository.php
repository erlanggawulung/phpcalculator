<?php

namespace Jakmall\Recruitment\Calculator\Helper;
use Jakmall\Recruitment\Calculator\Model\Log;

class LogRepository {
    protected $FILENAME = "log.txt";

    public function __construct()
    {

    }

    public function save(Log $log)
    {
        $logFile = fopen($this->FILENAME, "a");
        $row = $log->getRow();
        fwrite($logFile, $row);
        fclose($logFile);
    }

    public function read($commands)
    {
        $logFile = fopen($this->FILENAME, "r");
        $results = [];
        while(!feof($logFile))  {
            $result = fgets($logFile);
            $splittedResult = explode(",",$result);
            if(in_array($splittedResult[0], $commands)) {
                array_push($results, $result);
            }
        }
        fclose($logFile);
        return $results;
    }

    public function clear()
    {
        $logFile = fopen($this->FILENAME, "w");
        $row = "";
        fwrite($logFile, $row);
        fclose($logFile);
    }
}