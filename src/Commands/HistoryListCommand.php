<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Helper\LogRepository;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $logRepository;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {commands?* : Filter history by commands}',
            $commandVerb
        );
        $this->description = sprintf('Show calculator history');

        $this->logRepository = new LogRepository();

        parent::__construct();
    }

    protected function getInput(): array
    {
        return $this->argument('commands');
    }

    protected function getCommandVerb(): string
    {
        return 'history:list';
    }
    
    public function handle(): void
    {
        $commands = $this->getInput();
        $mask = "|%3s |%-13.30s |%-35.30s |%6s |%-35.30s |%-20.30s |\n";
        $results = $this->logRepository->read($commands);
        if(sizeof($results) <= 0) {
            $this->comment('History is empty');
        } else {
            printf($mask, 'No', 'Command','Description','Result','Output','Time');
            for($i = 0; $i < sizeof($results); $i++) {
                $result = explode(",",$results[$i]);
                $result[4] = str_replace("\n", "", $result[4]);
                printf($mask, $i+1, $result[0],$result[1],$result[2],$result[3],$result[4]);
            }
        }
    }
}
