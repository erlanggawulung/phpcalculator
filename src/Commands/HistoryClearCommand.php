<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Helper\LogRepository;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $logRepository;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s',
            $commandVerb
        );
        $this->description = sprintf('Clear saved history');

        $this->logRepository = new LogRepository();

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:clear';
    }
    
    public function handle(): void
    {
        $this->logRepository->clear();
        $this->comment('History cleared!');
    }
}
