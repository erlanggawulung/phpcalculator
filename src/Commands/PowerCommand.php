<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Model\Log;
use Jakmall\Recruitment\Calculator\Helper\LogRepository;

class PowerCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    protected $logRepository;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {base : The base number} {exp : The exponent number}',
            $commandVerb
        );
        $this->description = sprintf('%s the given number', 'Exponent');

        $this->logRepository = new LogRepository();

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'powered';
    }

    public function handle(): void
    {
        $commandVerb = $this->getCommandVerb();
        $base = $this->getBase();
        $exp = $this->getExp();
        $description = $this->generateCalculationDescription($base, $exp);
        $result = $this->calculateAll($base, $exp);

        $log = new Log(ucfirst($commandVerb), $description, $result);
        $this->logRepository->save($log);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getBase(): string
    {
        return $this->argument('base');
    }

    protected function getExp(): string
    {
        return $this->argument('exp');
    }

    protected function generateCalculationDescription($base, $exp): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return $base .' '.$operator.' '.$exp;
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll($base, $exp)
    {
        return $this->calculate($base, $exp);
    }

    /**
     * @param int|float $base
     * @param int|float $exp
     *
     * @return int|float
     */
    protected function calculate($base, $exp)
    {
        return pow($base, $exp);
    }
}
