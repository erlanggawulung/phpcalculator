<?php

namespace Jakmall\Recruitment\Calculator\Model;

class Log {
    protected $command;
    protected $description;
    protected $result;
    protected $output;
    protected $time;

    public function __construct($command, $description, $result)
    {
        $this->command = $command;
        $this->description = $description;
        $this->result = $result;
        $this->output = $description." = ".$result;
        $this->time = date("Y-m-d H:i:s");
    }

    public function getRow()
    {
        return $this->command.",".$this->description.",".$this->result.",".$this->output.",".$this->time."\n";
    }
}